$(document).ready(function () {
	$('.carouselBestEvents').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
				margin: 10,
			},
			1024: {
				items: 4,
			},
			1950: {
				items: 4,
			},
		},
	});

	$('.owl-carousel').each(function () {
		var owl = $(this);
		owl.on('click', '.owl-item', function (event) {
			var currentItem = $(this);
			var activeItem = owl.find('.owl-item.active');

			if (!currentItem.hasClass('active')) {
				event.preventDefault();

				if (currentItem.index() < activeItem.index()) {
					owl.trigger('prev.owl.carousel', [300]);
				} else {
					owl.trigger('next.owl.carousel', [300]);
				}
			}
		});
	});
});
