$(document).ready(function () {
	$('.placeTipImg').mouseover(function () {
		$('body').addClass('placeTipActive');
	});

	$('.placeTipX').click(function () {
		$('body').removeClass('placeTipActive');
	});

	$('.clearButton').click(function () {
		$('.selectName').val('');
	});

	$('.backgroundModalSearch').click(function () {
		$('body').removeClass('searchEventsModalActive');
	});

	$('.searchModalButton').click(function () {
		$('body').addClass('searchEventsModalActive');
		$('body').removeClass('placeTipActive');
	});

	$('.searchModalClose').click(function () {
		$('body').removeClass('searchEventsModalActive');
	});

	$('.go').click(function () {
		$('main').addClass('filter');
		$('.firstContent').addClass('fixed');
		$('body').addClass('filteredContent');
		$('.launchOnTheSite').addClass('disabled');
		$('header').removeClass('bigLogo');
		$('footer').addClass('hide');
		$('.events .filter .filterList').addClass('.filterList');
	});

	$('.mapChange').click(function () {
		$('main').addClass('filter events-map');
		$('.firstContent').addClass('fixed');
		$('body').addClass('filteredContent');
		$('body').addClass('mapActive');
		$('header').removeClass('bigLogo');
		$('footer').addClass('hide');
		$('.events .filter .filterList').addClass('.filterList');
	});

	$('.listChange').click(function () {
		$('main').removeClass('events-map');
		$('main').addClass('filter');
		$('.firstContent').addClass('fixed');
		$('body').addClass('filteredContent');
		$('body').removeClass('mapActive');
		$('header').removeClass('bigLogo');
		$('footer').addClass('hide');
		$('.events .filter .filterList').addClass('.filterList');
	});

	$('.carouselEvents').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		margin: 20,
		responsiveClass: true,
		navContainerClass: 'navRegular owl-nav',
		nav: true,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			800: {
				items: 1,
				nav: false,
			},
			1024: {
				items: 2,
			},
			1950: {
				items: 2,
			},
		},
	});

	$('.carouselBestEvents').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		navContainerClass: 'navRegular owl-nav',
		nav: true,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			1024: {
				items: 4,
			},
			1950: {
				items: 4,
			},
		},
	});

	$('.owl-carousel').each(function () {
		var owl = $(this);
		owl.on('click', '.owl-item', function (event) {
			var currentItem = $(this);
			var activeItem = owl.find('.owl-item.active');

			if (!currentItem.hasClass('active')) {
				event.preventDefault();

				if (currentItem.index() < activeItem.index()) {
					owl.trigger('prev.owl.carousel', [300]);
				} else {
					owl.trigger('next.owl.carousel', [300]);
				}
			}
		});
	});

	$('.nameButton').on('click', function (event) {
		event.preventDefault();
		$('.selectName')[0].sumo.unSelectAll();
	});
	$('.placeButton').on('click', function (event) {
		event.preventDefault();
		$('.selectPlace')[0].sumo.unSelectAll();
	});
	$('.showButton').on('click', function (event) {
		event.preventDefault();
		$('.selectShow')[0].sumo.unSelectAll();
	});
	$('.typeButton').on('click', function (event) {
		event.preventDefault();
		$('.selectType')[0].sumo.unSelectAll();
	});
	$('.genderButton').on('click', function (event) {
		event.preventDefault();
		$('.selectGender')[0].sumo.unSelectAll();
	});

	$('.moreItems').click(function (e) {
		e.preventDefault();
		$('.item').removeClass('disabled');
		$(this).hide();
	});

	if ($(window).width() <= 1024) {
		$('.listItemsCarousel').addClass('owl-carousel');
		$('.listItemsCarousel').addClass('owl-theme');
		$('.listItemsCarousel').addClass('carouselNextEvents');
	}

	$('.owl-carousel').each(function () {
		var owl = $(this);
		owl.on('click', '.owl-item', function (event) {
			var currentItem = $(this);
			var activeItem = owl.find('.owl-item.active');

			if (!currentItem.hasClass('active')) {
				event.preventDefault();

				if (currentItem.index() < activeItem.index()) {
					owl.trigger('prev.owl.carousel', [300]);
				} else {
					owl.trigger('next.owl.carousel', [300]);
				}
			}
		});
	});

	$('.carouselNextEvents').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		margin: 10,
		responsiveClass: true,
		nav: false,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
				margin: 10,
			},
			1024: {
				items: 4,
			},
			1950: {
				items: 4,
			},
		},
	});
});
