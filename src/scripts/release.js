$(document).ready(function () {
	$('.goSearch').click(function () {
		$('body').addClass('filterSearchMode');
		$('body').removeClass('searchEventsModalActive');
		if ($(window).width() <= 1024) {
			$('.advertisement').addClass('disabled');
		}
	});

	$('.searchModalButton').click(function () {
		$('body').addClass('searchEventsModalActive');
		$('.searchModalButton').addClass('disabled');
		$('body').removeClass('placeTipActive');
	});

	$('.searchModalClose').click(function () {
		$('body').removeClass('searchEventsModalActive');
		$('.searchModalButton').removeClass('disabled');
	});

	$('input[name="daterange"]').daterangepicker({
		autoApply: true,
	});

	$('.carouselRelease').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		navContainerClass: 'navDark owl-nav',
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
				margin: 10,
			},
			450: {
				items: 1,
				nav: false,
				margin: 10,
			},
			940: {
				items: 2,
				nav: false,
				margin: 10,
			},
			1025: {
				items: 4,
			},
			1500: {
				items: 4,
			},
		},
	});
});
