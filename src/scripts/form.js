$(document).ready(function () {
	$('input[name="daterange"]').daterangepicker({
		autoApply: true,
	});

	$('.clearButton').click(function (event) {
		event.preventDefault();
		$(this).closest('.field').find('.text').val('');
	});

	$('input[name="daterange"]').val('');
	$('input[name="daterange"]').attr('placeholder', 'Selecione a data');

	$('.placeTipImg').mouseover(function () {
		$('body').addClass('placeTipActive');
	});

	$('.placeTipX').click(function () {
		$('body').removeClass('placeTipActive');
	});
});
