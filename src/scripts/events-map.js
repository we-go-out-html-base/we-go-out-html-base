$(document).ready(function () {
	$('.placeTipImg').mouseover(function () {
		$('body').addClass('placeTipActive');
	});

	$('.placeTipX').click(function () {
		$('body').removeClass('placeTipActive');
	});

	if ($(window).width() <= 1024) {
		$('.listItemsCarousel').addClass('owl-carousel');
		$('.listItemsCarousel').addClass('owl-theme');
		$('.listItemsCarousel').addClass('carouselEvents');
	}

	$('.owl-carousel').each(function () {
		var owl = $(this);
		owl.on('click', '.owl-item', function (event) {
			var currentItem = $(this);
			var activeItem = owl.find('.owl-item.active');

			if (!currentItem.hasClass('active')) {
				event.preventDefault();

				if (currentItem.index() < activeItem.index()) {
					owl.trigger('prev.owl.carousel', [300]);
				} else {
					owl.trigger('next.owl.carousel', [300]);
				}
			}
		});
	});

	$('.carouselEvents').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		margin: 10,
		responsiveClass: true,
		nav: false,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
				margin: 10,
			},
			1024: {
				items: 4,
			},
			1950: {
				items: 4,
			},
		},
	});

	$('input[name="daterange"]').daterangepicker({
		autoApply: true,
	});
});
