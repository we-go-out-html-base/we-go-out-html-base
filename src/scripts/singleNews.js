$(document).ready(function () {
	$('.indexButton').click(function () {
		$('body').toggleClass('menuExtraActive');
	});

	$('.backgroundModalIndex').click(function () {
		$('body').removeClass('menuExtraActive');
	});

	$('.link').click(function (event) {
		event.preventDefault();
		var currentURL = window.location.href;

		var tempInput = $('<input>');
		tempInput.val(currentURL);

		$('body').append(tempInput);
		tempInput.select();
		document.execCommand('copy');
		tempInput.remove();

		$('body').addClass('modalAlertActive');

		setTimeout(function () {
			$('body').removeClass('modalAlertActive');
		}, 3000);
	});

	$('.carouselGuide').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		navContainerClass: 'navDark owl-nav',
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			700: {
				items: 2,
				nav: false,
			},
			950: {
				items: 3,
			},
			1350: {
				items: 4,
			},
			1500: {
				items: 4,
			},
		},
	});

	$('span').click(function () {
		var $content = $(this).closest('.content');

		if ($content.length > 0) {
			var imgSrc = $(this).siblings('img').attr('src');
			var modalContent = $('<img>').attr('src', imgSrc);
			var closeButton = $('<button>').addClass('close-button').html('&times;');
			var figCaption = $(this).parent().find('figcaption').clone();
			var modalInner = $('<div>')
				.addClass('modalContent-inner')
				.append(modalContent, figCaption, closeButton);
			var modal = $('<div>').addClass('modalContent').append(modalInner);

			closeButton.click(function () {
				modal.removeClass('show');
				setTimeout(function () {
					modal.remove();
				}, 2000);
			});

			modal.click(function (e) {
				if (e.target === this) {
					modal.removeClass('show');
					setTimeout(function () {
						modal.remove();
					}, 2000);
				}
			});

			$('body').append(modal);

			// Ativa o modal com o efeito de transição
			setTimeout(function () {
				modal.addClass('show');
			}, 50);
		}
	});
});
