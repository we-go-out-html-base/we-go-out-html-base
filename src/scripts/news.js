$(document).ready(function () {
	$('.goSearch').click(function () {
		$('body').addClass('filterSearchMode');
		if ($(window).width() <= 1024) {
			$('.advertisement').addClass('disabled');
		}
	});

	$('.categoryButton').on('click', function (event) {
		event.preventDefault();
		$('.selectCategory')[0].sumo.unSelectAll();
	});

	$('.moreItems').click(function (e) {
		e.preventDefault();
		$('.item').removeClass('disabled');
		$(this).hide();
	});
});
