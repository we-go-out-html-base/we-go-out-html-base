$(document).ready(function () {
	$('.carouselFestivals').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: false,
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			1025: {
				items: 2,
			},
		},
	});

	$('.carouselPartners').owlCarousel({
		navSpeed: 500,
		loop: false,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		navContainerClass: 'navDark owl-nav',
		dots: false,
		autoWidth: false,
		responsive: {
			0: {
				items: 3,
				nav: false,
			},
			450: {
				items: 3,
				nav: false,
			},
			1025: {
				items: 6,
			},
		},
	});

	$('.owl-carousel').each(function () {
		var owl = $(this);
		owl.on('click', '.owl-item', function (event) {
			var currentItem = $(this);
			var activeItem = owl.find('.owl-item.active');

			if (!currentItem.hasClass('active')) {
				event.preventDefault();

				if (currentItem.index() < activeItem.index()) {
					owl.trigger('prev.owl.carousel', [300]);
				} else {
					owl.trigger('next.owl.carousel', [300]);
				}
			}
		});
	});
});
